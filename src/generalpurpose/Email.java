package generalpurpose;


import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Properties;

public class Email {
    static final String ENCODING = "UTF-8";

    public static void sendMessage( String subject, String mail_text, ArrayList<String> attachment, String address) throws MessagingException, UnsupportedEncodingException {
        Properties props = System.getProperties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", MainConfig.smtpHost);
        props.put("mail.smtp.port", MainConfig.smtpPort);
        props.put("mail.mime.charset", ENCODING);
        Authenticator auth = new MyAuthenticator(MainConfig.login, MainConfig.password);
        Session session = Session.getDefaultInstance(props, auth);
        MimeMessage msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(MainConfig.login));
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress(address));
        msg.setSubject(subject, ENCODING);
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(mail_text, "text/plain; charset=" + ENCODING + "");
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
        int amount_attachments = attachment.size();
        if ( MainConfig.limit_attachments < amount_attachments) {
            amount_attachments = MainConfig.limit_attachments;
        }
        for ( int number_attachment = 0; number_attachment <  amount_attachments; number_attachment++ ) {
            if (( attachment.get( number_attachment ) == "Error" ) | ( attachment.get( number_attachment ) == "Success" ) ) {
                continue;
            }
            MimeBodyPart attachmentBodyPart = new MimeBodyPart();
            DataSource source = new FileDataSource( attachment.get( number_attachment ) );
            attachmentBodyPart.setDataHandler( new DataHandler( source ) );
            attachmentBodyPart.setFileName( MimeUtility.encodeText( source.getName()));
            multipart.addBodyPart( attachmentBodyPart );
        }
        msg.setContent( multipart );
        Transport.send( msg );
        System.out.println( "Message is sent.");
    }
}

class MyAuthenticator extends Authenticator {
    private String user;
    private String password;

    MyAuthenticator(String user, String password) {
        this.user = user;
        this.password = password;
    }

    public PasswordAuthentication getPasswordAuthentication() {
        String user = this.user;
        String password = this.password;
        return new PasswordAuthentication(user, password);
    }
}
