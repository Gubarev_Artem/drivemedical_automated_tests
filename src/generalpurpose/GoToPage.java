package generalpurpose;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class GoToPage {
    public static void goTogoToMainPageB2B( WebDriver driver, WebDriverWait wait_browser) {
        driver.get( MainConfig.main_page_b2b_url);
        wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( MainConfig.title_product_catalog)));
    }
}
