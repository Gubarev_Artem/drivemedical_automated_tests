package generalpurpose;


import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MakeScreenshot {
    public static String screenshotFailTest( ITestResult result, int amount_products, WebDriver driver) {
        System.out.println( "\nCreate screenshot failing test. Check images: " + amount_products + "\n");
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");
        String methodName = result.getName();
        if(!result.isSuccess()){
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            try {
                String path = "failure_screenshots/" + methodName + "_" + formater.format(calendar.getTime()) + ".png";
                FileUtils.copyFile(scrFile, new File( path ));
                return path;
            }
            catch (IOException e1) {
                System.out.println("\nError create image file. Screenshot is not created.\n");
                return "Error";
            }
        }
        return "Error";
    }

    public static String screenshotPage( String name_poduct, int amount_products, WebDriver driver) {
        System.out.println( "\nCreate screenshot fail image. Check images: " + amount_products + "\n");
        Calendar calendar = Calendar.getInstance();
        name_poduct = name_poduct.replaceAll("[^\\dA-Za-z ]", "").replaceAll("\\s+", "_");
        SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            String path = "no_image_screenshots/" + name_poduct + "_" + formater.format(calendar.getTime()) + ".png";
            FileUtils.copyFile(scrFile, new File( path ));
            return path;
        }
        catch (IOException e1) {
            System.out.println("\nError create image file. Screenshot is not created.\n");
            return "Error";
        }
    }

    public static String screenshotPageWarning( int amount_products, WebDriver driver) {
        System.out.println( "\nCreate screenshot warning situation. Check images: " + amount_products + "\n");
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            String path = "warning_screenshots/" + "_" + formater.format(calendar.getTime()) + ".png";
            FileUtils.copyFile(scrFile, new File( path ));
            return path;
        }
        catch (IOException e1) {
            System.out.println("\nError create image file. Screenshot is not created.\n");
            return "Error";
        }
    }
}
