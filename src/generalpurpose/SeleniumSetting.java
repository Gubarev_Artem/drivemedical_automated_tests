package generalpurpose;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class SeleniumSetting extends Assert {
    public static WebDriver seleniumSetup( String browser) {
        WebDriver driver= new FirefoxDriver();
        driver.close();
        String os = System.getProperty("os.name").toLowerCase();
        if ( browser.equals( "Firefox")) {
            driver = new FirefoxDriver();
        }
        else if ( browser.equals("Chrome") & os.contains("win")) {
            System.setProperty( "webdriver.chrome.driver", "C:\\chromedriver.exe");
            driver = new ChromeDriver();
        }
        else if ( browser.equals("Chrome") & os.contains("nux")) {
            System.setProperty( "webdriver.chrome.driver", "/home/selenium/chromedriver.exe");
            driver = new ChromeDriver();
        }
        else {
            assertTrue( false, "Not selected the default browser");
        }
        driver.manage().timeouts().pageLoadTimeout(MainConfig.default_timeout, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(MainConfig.default_timeout, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(MainConfig.default_timeout, TimeUnit.SECONDS);
        driver.get( MainConfig.main_page_b2b_url);
        if( checkurl( driver) == false) {
            String message = "REQUEST URL (%s) and GETTING URL (%s) are different";
            System.out.println( message.format("REQUEST URL (%s) and GETTING URL (%s) are different", MainConfig.main_page_b2b_url, driver.getCurrentUrl()));
            driver.quit();

        }
        return driver;
    }

    private static boolean checkurl ( WebDriver driver) {
        String url = driver.getCurrentUrl();
        if ( url.contains( MainConfig.main_page_b2b_url )) {
            return true;
        }
        return false;
    }

}
