package imagesofproducts;

public class CheckImagesOfProductsConfig {



    public static String left_product_menu_xpath = "//ul[@id='business-menu']//a[@title]/span";
    public static String left_secondary_product_menu_xpath = "//div[@class='n2-banner-right']/ul/li/a";
    public static String amount_show_all_product_xpath = "//select/option[contains(text(),'All')]";
    public static String placeholder_small_image = "/placeholder/small_image.jpg";
    public static String header_product_xpath = "//div[@class='product-name']/h1";
    public static String models_products_xpath = "//span[@class = 'price']/span";
    public static String image_on_the_product_page_xpath = "//p[contains (@class, 'product-image')]/img";
    public static String more_views_images_xpath = "//div[@class='more-views']//ul/li/a";
    public static String button_close_window_xpath = "//div[1]/a/span[contains(text(),'Close Window')]";
    public static String image_popup_css = "#product-gallery-image";
    public static String placeholder_image = "/placeholder/image.jpg";
    public static String third_level_category_products_xpath = "//div[@class='product-image']/a/img";
    public static String names_third_level_category_products_xpath = "//h6/a";
    public static String products_xpath = "//ol[@id='products-list']//a/img";
    public static String names_products_xpath = "//ol[@id='products-list']//h2/a";
    public static String next_page_product_xpath = "//img[@alt='Next']";
}
