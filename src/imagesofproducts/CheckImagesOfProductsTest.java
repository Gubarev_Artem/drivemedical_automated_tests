package imagesofproducts;


import generalpurpose.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import javax.mail.MessagingException;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.*;
import org.testng.annotations.*;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;


public class CheckImagesOfProductsTest extends Assert {

    private int general_amount_products= 0;
    private int number_message = 0;
    private String mail_text = "\nTest results of product images\n";
    private ArrayList<String> attachments = new ArrayList<String>();
    private boolean flag_success = false;
    private boolean flag_bad_result = false;


    @AfterClass
    @Parameters("email_address")
    public void tearDown( String address) throws UnsupportedEncodingException, MessagingException {
        if ( flag_bad_result  == false ) {
            addToEmail( "Success", "Checked all product images.");
        }
        else addToEmail( "Error", "Not all images are checked.");
        Email.sendMessage( MainConfig.subject, mail_text, attachments, address );
    }

    @AfterMethod
    public void tearEnd( ) {
        if ( flag_success == false ) {
            flag_bad_result = true;
        }
    }

    @Parameters({"start_category_part1", "end_category_part1", "browser"})
    @Test( description = "Test search and check product images part 1", enabled = true )
    public void testCheckAllImagesPart1( int start_category_part1, int end_category_part1, String browser) throws Exception {
        WebDriver driver = SeleniumSetting.seleniumSetup( browser);
        WebDriverWait wait_browser = new WebDriverWait( driver, MainConfig.default_timeout);
        CheckAllImages( driver, wait_browser, start_category_part1, end_category_part1);
    }

    @Parameters({"start_category_part2", "end_category_part2", "browser"})
    @Test( description = "Test search and check product images part 2", enabled = true )
    public void testCheckAllImagesPart2( int start_category_part2, int end_category_part2, String browser) throws Exception {
        WebDriver driver = SeleniumSetting.seleniumSetup( browser);
        WebDriverWait wait_browser = new WebDriverWait( driver, MainConfig.default_timeout);
        CheckAllImages( driver, wait_browser, start_category_part2, end_category_part2);
    }

    @Parameters({"start_category_part3", "end_category_part3", "browser"})
    @Test( description = "Test search and check product images part 3", enabled = true )
    public void testCheckAllImagesPart3( int start_category_part3, int end_category_part3, String browser) throws Exception {
        WebDriver driver = SeleniumSetting.seleniumSetup( browser);
        WebDriverWait wait_browser = new WebDriverWait( driver, MainConfig.default_timeout);
        CheckAllImages( driver, wait_browser, start_category_part3, end_category_part3);
    }

    private void CheckAllImages( WebDriver driver, WebDriverWait wait_browser, int start_category, int end_category) throws Exception {
        try {
            GoToPage.goTogoToMainPageB2B( driver, wait_browser);
            // receive products categories
            wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckImagesOfProductsConfig.left_product_menu_xpath)));
            List<WebElement> list_product_categories = driver.findElements( By.xpath( CheckImagesOfProductsConfig.left_product_menu_xpath));
            if ( list_product_categories.size() == 0) {
                // Add message in report
                System.out.println( "Categories product catalog are not found. Checking the images is not made. ");
                String screenshot_path = MakeScreenshot.screenshotPageWarning( general_amount_products, driver );
                String image_url = driver.getCurrentUrl();
                addToEmail( screenshot_path, "Warning: category products list is empty. Test is stopped.", image_url);
                System.exit(0);
            }
            int amount_product_categories = list_product_categories.size();
            for ( int number_category = start_category; number_category < end_category; number_category++) {
                wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckImagesOfProductsConfig.left_product_menu_xpath)));
                list_product_categories = driver.findElements( By.xpath( CheckImagesOfProductsConfig.left_product_menu_xpath));
                WebElement category = list_product_categories.get( number_category);
                category.click();
                try {
                    wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckImagesOfProductsConfig.left_secondary_product_menu_xpath)));
                }
                catch ( org.openqa.selenium.TimeoutException e ) {
                    String screenshot_path = MakeScreenshot.screenshotPageWarning( general_amount_products, driver );
                    String image_url = driver.getCurrentUrl();
                    addToEmail( screenshot_path, "Warning: Product Categories second level are not found. Test is stopped.", image_url);
                    System.exit(0);
                }
                wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckImagesOfProductsConfig.left_secondary_product_menu_xpath)));
                List<WebElement> list_product_subcategories = driver.findElements( By.xpath( CheckImagesOfProductsConfig.left_secondary_product_menu_xpath));
                if ( list_product_subcategories.size() == 0) {
                    // Add message in report
                    System.out.println( "Subcategories product catalog are not found. Checking the images is not made. ");
                    String screenshot_path = MakeScreenshot.screenshotPageWarning( general_amount_products, driver);
                    String image_url = driver.getCurrentUrl();
                    addToEmail( screenshot_path, "Warning: subcategory products list is empty.", image_url);
                    continue;
                }
                int amount_product_subcategories = list_product_subcategories.size();
                for ( int number_subcategory = 0; number_subcategory < amount_product_subcategories; number_subcategory++) {
                    wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckImagesOfProductsConfig.left_secondary_product_menu_xpath)));
                    list_product_subcategories = driver.findElements( By.xpath( CheckImagesOfProductsConfig.left_secondary_product_menu_xpath));
                    WebElement subcategory = list_product_subcategories.get( number_subcategory);
                    subcategory.click();
                    wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckImagesOfProductsConfig.left_product_menu_xpath)));
                    showAllProducts( driver);
                    // Get list of products and list of names products
                    wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( MainConfig.title_product_catalog)));
                    List<WebElement> list_products = driver.findElements( By.xpath( CheckImagesOfProductsConfig.products_xpath));
                    if ( list_products.size() == 0) {
                        // If is not products.
                        checkImagesInSubdirectoryDepthOfFour( driver, wait_browser);
                    }
                    else {
                        checkImagesInListProducts( driver, wait_browser);
                    }
                    wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckImagesOfProductsConfig.left_product_menu_xpath)));
                    list_product_categories = driver.findElements( By.xpath( CheckImagesOfProductsConfig.left_product_menu_xpath));
                    category = list_product_categories.get( number_category );
                    category.click();
                    wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckImagesOfProductsConfig.left_secondary_product_menu_xpath)));
                }
                GoToPage.goTogoToMainPageB2B( driver, wait_browser );
            }
            driver.close();
            flag_success = true;
        }
        catch( org.openqa.selenium.TimeoutException e) {
            mail_text = mail_text + "\n " + " DriveMedical host is not responding!" + "\n";
            driver.close();
        }
    }

    private void checkImagesInSubdirectoryDepthOfFour( WebDriver driver, WebDriverWait wait_browser) {
        wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckImagesOfProductsConfig.third_level_category_products_xpath)));
        List<WebElement> list_product_subcategories = driver.findElements( By.xpath( CheckImagesOfProductsConfig.third_level_category_products_xpath));
        if ( list_product_subcategories.size() == 0) {
            // Add message in report
            System.out.println("Product subcategories or products are not founded.");
            String screenshot_path= MakeScreenshot.screenshotPageWarning( general_amount_products, driver );
            String image_url = driver.getCurrentUrl();
            addToEmail( screenshot_path, "Warning: subcategory products list is empty.", image_url);
            return;
        }
        int amount_subcategories = list_product_subcategories.size();
        // Create list names subcategories
        wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckImagesOfProductsConfig.names_third_level_category_products_xpath)));
        List<WebElement> list_names_subcategories = driver.findElements( By.xpath( CheckImagesOfProductsConfig.names_third_level_category_products_xpath));
        String[] names_subcategories = new String[amount_subcategories];
        for(int i=0;i<amount_subcategories;i++) {
            names_subcategories[i] = list_names_subcategories.get(i).getAttribute( "title");
        }
        // Check images subcategories
        for ( int number_subcategory = 0; number_subcategory < amount_subcategories; number_subcategory++) {
            wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckImagesOfProductsConfig.third_level_category_products_xpath)));
            list_product_subcategories = driver.findElements( By.xpath( CheckImagesOfProductsConfig.third_level_category_products_xpath));
            String source_image_on_product_page = list_product_subcategories.get( number_subcategory).getAttribute( "src");
            // if true create report
            if ( source_image_on_product_page.contains( CheckImagesOfProductsConfig.placeholder_image) |
                    source_image_on_product_page.contains( CheckImagesOfProductsConfig.placeholder_small_image)) {
                String screenshot_path = MakeScreenshot.screenshotPage( "Subcategory: " +  names_subcategories[number_subcategory], general_amount_products, driver );
                String image_url = driver.getCurrentUrl();
                addToEmail( names_subcategories[number_subcategory], screenshot_path, image_url);
            }
        }
        // Check images in subcategories
        String back_url = driver.getCurrentUrl();
        for ( int number_subcategory = 0; number_subcategory < amount_subcategories; number_subcategory++) {
            wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckImagesOfProductsConfig.third_level_category_products_xpath)));
            list_product_subcategories = driver.findElements( By.xpath( CheckImagesOfProductsConfig.third_level_category_products_xpath));
            WebElement subcategory = list_product_subcategories.get( number_subcategory);
            subcategory.click();
            List<WebElement> list_products = driver.findElements( By.xpath( CheckImagesOfProductsConfig.products_xpath));
            if ( list_products.size() == 0) {
                // If is not products.
                checkImagesInSubdirectoryDepthOfFour( driver, wait_browser);
            }
            else {
                wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckImagesOfProductsConfig.models_products_xpath)));
                showAllProducts( driver);
                checkImagesInListProducts( driver, wait_browser);
            }
            driver.get(back_url);
        }
    }

    private void showAllProducts( WebDriver driver) {
        // Select show All product. If not go on
        try {
            driver.findElement( By.xpath( CheckImagesOfProductsConfig.amount_show_all_product_xpath)).click();
        }
        catch (Throwable NoSuchElementException) {
            // Does not exist element select All product.
        }
    }

    private void checkImagesInListProducts( WebDriver driver, WebDriverWait wait_browser) {
        wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath(  MainConfig.title_product_catalog)));
        List<WebElement> list_products = driver.findElements( By.xpath( CheckImagesOfProductsConfig.products_xpath));
        if ( list_products.size() == 0) {
            // Add message in report
            System.out.println( "List products are empty. Checking the images is not made. ");
            String screenshot_path = MakeScreenshot.screenshotPageWarning( general_amount_products, driver );
            String image_url = driver.getCurrentUrl();
            addToEmail( screenshot_path, "Warning: products list is empty.", image_url);
            return;
        }
        int amount_products = list_products.size();
        // Create list products names
        wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckImagesOfProductsConfig.names_products_xpath)));
        List<WebElement> list_name_poducts = driver.findElements( By.xpath( CheckImagesOfProductsConfig.names_products_xpath));
        String[] names_products = new String[amount_products];
        for(int i=0;i<amount_products;i++) {
            names_products[i] = list_name_poducts.get(i).getAttribute( "title").trim();
        }
        // Create list products models
        wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckImagesOfProductsConfig.models_products_xpath)));
        List<WebElement> list_products_models = driver.findElements( By.xpath( CheckImagesOfProductsConfig.models_products_xpath));
        String[] products_models = new String[amount_products];
        for(int i=0;i<amount_products;i++) {
            products_models[i] = list_products_models.get(i).getText().trim();
        }
        // Check images in list products
        String back_url = driver.getCurrentUrl();
        for ( int number_product = 0; number_product < amount_products; number_product++) {
            wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckImagesOfProductsConfig.products_xpath)));
            list_products = driver.findElements( By.xpath( CheckImagesOfProductsConfig.products_xpath));
            WebElement product = list_products.get( number_product);
            String source_image = product.getAttribute( "src" );
            // if true create report
            if ( source_image.contains( CheckImagesOfProductsConfig.placeholder_small_image)) {
                // Add message in report
                String screenshot_path = MakeScreenshot.screenshotPage( "Product: " + names_products[number_product], general_amount_products, driver );
                String image_url = driver.getCurrentUrl();
                addToEmail( names_products[number_product], screenshot_path, products_models[number_product], image_url);
            }
            // Enter in product page
            product.click();
            wait_browser.until( ExpectedConditions.presenceOfElementLocated( By.xpath( CheckImagesOfProductsConfig.header_product_xpath)));
            // Check main image
            String source_image_on_product_page = driver.findElement( By.xpath( CheckImagesOfProductsConfig.image_on_the_product_page_xpath)).getAttribute( "src");
            // if true create report
            if ( source_image_on_product_page.contains( CheckImagesOfProductsConfig.placeholder_image)) {
                // Add message in report
                String screenshot_path = MakeScreenshot.screenshotPage( "Product: " + names_products[number_product], general_amount_products, driver );
                String image_url = driver.getCurrentUrl();
                addToEmail( names_products[number_product], screenshot_path, products_models[number_product],image_url);
            }
            general_amount_products++;
            driver.get(back_url);
        }
    }

    private void addToEmail( String product_name, String screenshot_path, String product_model, String url) {
        mail_text = mail_text + "Product image is not displayed: " + "\n";
        number_message++;
        attachments.add( screenshot_path );
        mail_text = mail_text + number_message + ". " + product_name + "\n" + product_model + "\n" + "URL: " + url + "\n\n";
    }

    private void addToEmail( String screenshot_path, String event, String url ) {
        attachments.add( screenshot_path );
        number_message++;
        mail_text = mail_text + number_message + ". " + event + "\n" + "URL: " + url + "\n\n";
    }

    private void addToEmail( String event ) {
        number_message++;
        mail_text = mail_text + number_message + ". " + event + "\n" + "\n\n";
    }

    private void addToEmail( String flag, String event) {
        attachments.add( flag );
        mail_text = mail_text + "\n " + event + "\n";
    }
}
